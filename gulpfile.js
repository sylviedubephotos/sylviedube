var sourcemaps = require('gulp-sourcemaps');
var scss = require('gulp-scss');
var autoprefix = require('gulp-autoprefixer');
var csso = require('gulp-csso');

gulp.task('style', function() {
   gulp.src('src/scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(scss())
      .pipe(autoprefix({
        browsers: ['last 2 versions']
      }))
      .pipe(csso())
    .pipe(sourcemaps.write('../maps'))
    .pipe(gulp.dest('dist'));
});

gulp.task('html', function () {
  gulp.src('src/**/*.html')
    
    .pipe(gulp.dest('dist'));
});

gulp.task('default', ['style', 'html']);
